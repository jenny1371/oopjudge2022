/** author: b07703125 Chia-Chien, Liu*/
/** Description: to create all kinds of Shape for main class*/
public class ShapeFactory {
	/** contains 3 kinds of shapes as values.*/
    enum Type{
    Circle,
    Square,
    Triangle
    }
    /** shapeType can refer to Triangle, Square or Circle while length represents the side length or diameter of the shape,
     *  returns an object which 'is-a' Shape object.*/
    public Shape createShape(ShapeFactory.Type shapeType, double length){
        Shape shapeObj;
        //determine the type of shape
        switch (shapeType){
            case Circle:
                shapeObj = new Circle(length);
                break;
            case Square:
                shapeObj = new Square(length);
                break;
            default:
                shapeObj = new Triangle(length);
        }
        return shapeObj;
    }
}
