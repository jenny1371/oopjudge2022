/** author: b07703125 Chia-Chien, Liu*/
/** inherits from shape and override its functions*/
public class Square extends Shape {
    public Square(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
        super.length = length;
    }

    @Override
    public double getArea() {
        return (double) Math.round(length*length*100)/100;
    }

    @Override
    public double getPerimeter() {
        return (double) Math.round(4*length*100)/100;
    }
}
