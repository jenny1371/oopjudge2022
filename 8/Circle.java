/** author: b07703125 Chia-Chien, Liu*/
/** inherits from shape and override its functions*/
public class Circle extends Shape {
    public Circle(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
        super.length = length;
    }

    @Override
    public double getArea() {
        return (double) Math.round(length*length*Math.PI/4*100)/100;
    }

    @Override
    public double getPerimeter() {
        return (double) Math.round(length*Math.PI*100)/100;
    }
}
