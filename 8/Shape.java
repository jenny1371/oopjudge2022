/** author: b07703125 Chia-Chien, Liu*/
/** provided, will be inherited  and override by square, triangle and circle*/
public abstract class Shape {
	/**Input*/
	/**stores the side length for regular triangle, square, and the diameter for circle. */
    protected double length;
    public Shape(double length){
        this.length=length;
    }
    /**set the length of this shape.*/
    public abstract void setLength(double length);
    
    /**Output*/
    /**get the area of this shape.*/
    public abstract double getArea();
    /**get the parameter of this shape.*/
    public abstract double getPerimeter();
    /**print the area and parameter of this shape.*/
    public String getInfo(){
        return "Area = "+getArea()+
                ", Perimeter = "+getPerimeter();
    }
}