/** author: b07703125 Chia-Chien, Liu*/
/** inherits from shape and override its functions*/
public class Triangle extends Shape{
    public Triangle(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
        super.length = length;
    }

    @Override
    public double getArea() {
        return (double) Math.round(length*length*Math.sqrt(3)/2/2*100)/100;
    }

    @Override
    public double getPerimeter() {
        return (double) Math.round(length*3*100)/100;
    }
}
