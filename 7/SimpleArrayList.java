/*** author:B07703125 Chia-Chien, Liu
*Description:SimpleArrayList uses an Integer array as its internal instance variable to save integer, and its size needs to be changed automatically based on user's manipulation. */
public class SimpleArrayList{
    Integer[] a;
    int sizeOfArray;
    /*** Default constructor that sets the array size to zero.*/
    public SimpleArrayList(){
        sizeOfArray = 0;
    }
    /*** Constructor that can set initial array size, and set all Integer to zero in the array.*/
    public SimpleArrayList(int initArraySize){
        sizeOfArray=initArraySize;
        a = new Integer[sizeOfArray];
        for(int i = 0 ; i<sizeOfArray ; i++){
            a[i] = 0;
        }
    }
    /*** appends the specified element to the end of this array.*/
    public void add(Integer i){
        sizeOfArray++;
        Integer[] tmp = new Integer[sizeOfArray];
        for(int j = 0; j<sizeOfArray-1 ; j++){
            tmp[j] = a[j];
        }
        tmp[sizeOfArray-1] = i;
        a = tmp;
    }
    /*** returns the element at the specified position in this array.If the specified position is out of range of the array, returns null.*/
    public Integer get(int index){
        if(index >= sizeOfArray || index < 0){
            return null;
        }
        return a[index];
    }
    /*** replaces the element at the specified position in this array with the specified element, and returns the original element at that specified position. If the specified position is out of range of the array, returns null.*/
    public Integer set(int index, Integer element){
        if(index>=sizeOfArray){
            return null;
        }
        Integer tmp = a[index];
        a[index] = element;
        return tmp;
    }
    /*** If a null element is at the specified position, returns false; otherwise removes it and returns true. Shifts any subsequent elements to the left if removes successfully.*/
    public boolean remove(int index){
        if(a[index] == null){
            return false;
        }
        sizeOfArray--;
        Integer[] tmp = new Integer[sizeOfArray];
        a[index] = null;
        for(int i = index; i<sizeOfArray ;i++){
            a[i] = a[i+1];
        }
        for(int i = 0; i<sizeOfArray;i++){
            tmp[i]=a[i];
        }
        a = tmp;
        return true;
    }
    /*** removes all of the elements from this array.*/
    public void clear(){
        a = null;
        sizeOfArray = 0;
    }
    /*** returns the number of elements in this array.*/
    public int size(){
        return sizeOfArray;
    }
    /*** retains only the elements in this array that are contained in the specified SimpleArrayList. In other words, removes from this array all of its elements that are not contained in the specified SimpleArrayList. Returns true if this array changed as a result.*/
    public boolean retainAll(SimpleArrayList l){
        boolean isChanged = false;
        SimpleArrayList checked = new SimpleArrayList();
        for(int i = 0; i < l.size() ; i++){
            boolean repeats = false;
            for(int j = 0; j < checked.size() ; j++){
                if(checked.get(j).equals(l.get(i))){
                    repeats = true;
                }
            }
            if(!repeats){
                checked.add(l.get(i));
            }
        }
        for(int i = 0; i<size();i++){
            boolean containsL = false;
            for(int j = 0 ; j < checked.size() ; j++){
                if(checked.get(j).equals(get(i))){
                    containsL = true;
                    break;
                }
            }
            if(!containsL){
                remove(i);
                isChanged = true;
            }
        }
        return isChanged;
    }
}
