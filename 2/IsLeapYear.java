/** author: b07703125 Chia-Chien, Liu/**
/** Discription: leap years are determined by years divisible to 4 and 400 except 100. */
public class IsLeapYear {
	public boolean determine(int year) {
	    if(year % 400 == 0)
	    {
	        return true;
	    }
	    else if(year % 100 == 0)
	    {
	        return false;
	    }
	    else if(year % 4 == 0)
	    {
	        return true;
	    }
	    return false;
	}
}
