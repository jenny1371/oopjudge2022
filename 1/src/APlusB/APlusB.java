package APlusB;
public class APlusB {
	public static java.lang.Integer plus(int a, int b) {
        /** The plus function works only if integer a and b are within the given range. */
        if ( a>=-1000000 && a<=1000000 && b>=-1000000 && b<=1000000 )
            return a+b;
        else
            return null;
    }
    public static void main(String[] args)  {
    	System.out.println(APlusB.plus(-100,99));
    }
}

