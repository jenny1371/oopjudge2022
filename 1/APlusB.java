/** author: b07703125 Chia-Chien, Liu/**
/** Discription: find the sum of integer a and b if both of them are within -1000000~1000000. */
public class APlusB {
	public static java.lang.Integer plus(int a, int b) {
        /** The plus function works only if integer a and b are within the given range. */
        if ( a>=-1000000 && a<=1000000 && b>=-1000000 && b<=1000000 )
            return a+b;
        /** If at least one of the integer a and b is out of range, return null. */
        else
            return null;
    }
    public static void main(String[] args)  {
	/** Try to print the input. */
    	System.out.println(APlusB.plus(-100,99));
    }
}

