/** author: b07703125 Chia-Chien, Liu*/
/**Description: It contains an enumerated type ExceptionTYPE which includes two kinds of exception. */
public class ATM_Exception extends Exception {
    enum ExceptionTYPE {
        BALANCE_NOT_ENOUGH,
        AMOUNT_INVALID
    }
    /**To record the detail of exception raised, we need a private variable excptionCondition with the type of ExceptionTYPE just defined,and then we set it by constructor. */
    private ExceptionTYPE excptionCondition;
    
    public ATM_Exception(ExceptionTYPE excptionCondition) {
        this.excptionCondition = excptionCondition;
    }
    /**To get the information of exception raised, use getMessage.*/
    public String getMessage() {
        return excptionCondition.toString();
    }
}
