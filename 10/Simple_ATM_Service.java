/** author: b07703125 Chia-Chien, Liu*/
/**Description: This simple ATM only provides service of withdrawals, this class implements given interface ATM_Service.*/
public class Simple_ATM_Service implements ATM_Service {
	/**help checking whether balance in user's account is sufficient, 
	 * if not, throws an exception named ATM_Exception with type of " BALANCE_NOT_ENOUGH"*/
	@Override
    public boolean checkBalance(Account account, int money) throws ATM_Exception {
        if(account.getBalance() >= money) {
            return true;
        }
        else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
        }
    }
	/**checks if amount of money can be divided by 1000, 
	 * if not, throws an exception named ATM_Exception with type of " AMOUNT_INVALID";*/
    @Override
    public boolean isValidAmount(int money) throws ATM_Exception {
        if(money%1000 == 0) {
            return true;
        }
        else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
        }
    }
    /**first calls checkBalance and then calls isValidAmount to check if it is a valid operation.
     * If valid, simple ATM will debt for amount of money the user specified ,and balance of user's account will also be updated. 
     * withdraw has to catch the exceptions raised by checkBalance and isValidAmount, and use getMessage defined in ATM_Exception to show the exception information.
     * At the end, it will always show updated balance in user's account in format of "updated balance : XXX", no matter whether the user withdraws the money successfully or not.*/
    @Override
    public void withdraw(Account account, int money) {
        try {
            checkBalance(account, money);
            isValidAmount(money);
            account.setBalance(account.getBalance()-money);
            System.out.println("updated balance : " + account.getBalance());
        } catch (ATM_Exception e) {
            System.out.println(e.getMessage());
            System.out.println("updated balance : " + account.getBalance());
        }
    }
}
