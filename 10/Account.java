/** author: b07703125 Chia-Chien, Liu*/
/**Description: Provided, used for account balance*/
class Account {
	private int balance;
	public Account(int balance) {
		setBalance(balance);
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
}
