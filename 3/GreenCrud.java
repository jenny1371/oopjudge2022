/** author: b07703125 Chia-Chien, Liu/**
 /** Discription: It is observed that the green crud population grows every five days, and that it grows in the same manner as the Fibonacci sequence does. */
public class GreenCrud{
    public int calPopulation(int initialSize, int days){
        int numberInSequence = days/5+1;//the number in the Sequence adds 1 every 5 days starting at day 1
        if(numberInSequence == 1 || numberInSequence == 2){//Fibonacci sequence: F(1)=F(2)=1
            return initialSize;
        }
        else{
            int first = 0;//line 8 to 15 finds the fibonacci number
            int second = 1;
            int nth = 1;
            for (int i = 2; i <= numberInSequence; i++) {
                nth = first + second;
                first = second;
                second = nth;
            }
            return nth*initialSize;//the number in the Fibonacci sequence times the initial size is the resulting size
        }
    }
}
