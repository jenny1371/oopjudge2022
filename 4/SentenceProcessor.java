/**
 * author:B07703125 Chia-Chien, Liu
 * Description: remove repeated words/ replace words with target in sentences.
 */
public class SentenceProcessor {
    public static String removeDuplicatedWords(String sentence){
        /**
         * Progressed word by word: check if is repeated(except 1st), and then save those not repeated to processedString.
         */
        String[] individualWords = sentence.split(" ");
        String[] processedSentence = new String [individualWords.length];
        processedSentence[0]=individualWords[0];
        StringBuilder processedString = new StringBuilder(processedSentence[0]);
        int wordCounter = 1;
        for (String word : individualWords){
            boolean isDuplicate = false;
            for (String check : processedSentence){
                if (check == null){
                    continue;
                }
                if (check.equals(word)){
                    isDuplicate = true;
                    break;
                }
            }

            if (!isDuplicate){
                processedSentence[wordCounter] = word;
                processedString.append(" ");
                processedString.append(word);
                wordCounter++;
            }
        }
        return processedString.toString();
    }
    public static String replaceWord(String target, String replacement, String sentence){
        /**
         * Progressed word by word: check if is target, then save by replacement to processedString; if is not, save by original words to processedString.
         */
        String[] individualWords = sentence.split(" ");
        StringBuilder processedString = new StringBuilder();
        for (String word : individualWords){
            if (word.equals(target)){
                processedString.append(replacement);
            }
            else {
                processedString.append(word);
            }
            processedString.append(" ");
        }
        processedString.deleteCharAt(processedString.length() - 1);
        return processedString.toString();
    }
}
