package hw;

public class APlusB {
	public static void main(String[] args) {
		System.out.println(APlusB.plus(-100, 99));
	}
	private static Object plus(int a, int b) {
		if (a>=-1000000 && a<=1000000 && b>=-1000000 && b<=1000000)
			return a+b;
		return null;
	}
}