/** author: b07703125 Chia-Chien, Liu*/
/** Discription: comes out pizza information. */
public class Pizza {
	/** Description: set variables */
	private String Size = "small";
	private int NumberOfCheese = 1;
	private int NumberOfPepperoni = 1;
	private int NumberOfHam = 1;
	/** Description: get all data in new. */
	public Pizza(String Size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam){
		this.Size = Size;
		this.NumberOfCheese = NumberOfCheese;
		this.NumberOfPepperoni = NumberOfPepperoni;
		this.NumberOfHam = NumberOfHam;
	}
    /** Description: initialize default information in constructor. */
	public Pizza() {
        Size = "small";
        NumberOfCheese = 1;
        NumberOfPepperoni = 1;
        NumberOfHam = 1;
    }
	/** Description: set variables and use get to return the value in one by one. */
	public String getSize() {
		return Size;
	}
	public void setSize(String Size) {
		this.Size = Size;
	}
	public int getNumberOfCheese() {
		return NumberOfCheese;
	}
	public void setNumberOfCheese(int NumberOfCheese) {
		this.NumberOfCheese = NumberOfCheese;
	}
	public int getNumberOfPepperoni() {
		return NumberOfPepperoni;
	}
	public void setNumberOfPepperoni(int NumberOfPepperoni) {
		this.NumberOfPepperoni = NumberOfPepperoni;
	}
	public int getNumberOfHam() {
		return NumberOfHam;
	}
	public void setNumberOfHam(int NumberOfHam) {
		this.NumberOfHam = NumberOfHam;
	}
//function
	public double calcCost() {
        int costOfSize;
        //learn!!!
        switch (Size){
            case "medium":
                costOfSize = 12;
                break;
            case "large":
                costOfSize = 14;
                break;
            //learn!!!!!
            default://cause original constructor calls small as default setting
                costOfSize = 10;
        }
        return costOfSize+(NumberOfCheese+NumberOfPepperoni+NumberOfHam)*2;
	}

	public String toString() {
		return "size = " + Size + ", numOfCheese = "+NumberOfCheese+", numOfPepperoni = "+NumberOfPepperoni+", numOfHam = "+NumberOfHam;
	}
	public boolean equals(Pizza otherPizza) {
        if(otherPizza.Size.equals(this.Size) && otherPizza.NumberOfHam==this.NumberOfHam &&
                otherPizza.NumberOfPepperoni==this.NumberOfPepperoni && otherPizza.NumberOfCheese==this.NumberOfCheese){
            return true;
        }
        else{
            return false;
        }
	}
}
