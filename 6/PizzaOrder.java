/** author: b07703125 Chia-Chien, Liu*/
/** Description: comes out pizza information. */
public class PizzaOrder {
	//learn Pizza object type!
    private Pizza pizza1;
    private Pizza pizza2;
    private Pizza pizza3;
    public PizzaOrder(){

    }
    /** Description: check if is valid number of pizza. */
    public boolean setNumberPizzas(int numberPizzas){
        return (numberPizzas<=3 && numberPizzas>=1);
    }
    /** Description: sets the pizza in the order. */
    public void setPizza1(Pizza pizza1){
        this.pizza1 = pizza1;
    }
    public void setPizza2(Pizza pizza2){
        this.pizza2 = pizza2;
    }
    public void setPizza3(Pizza pizza3){
        this.pizza3 = pizza3;
    }
    /** Description: calculates the total cost of the order.*/
    public double calcTotal(){
        double total = 0;
        if (pizza1 != null){
            total+=pizza1.calcCost();
        }
        if (pizza2 != null){
            total+=pizza2.calcCost();
        }
        if (pizza3 != null){
            total+=pizza3.calcCost();
        }
        return total;
    }
}
