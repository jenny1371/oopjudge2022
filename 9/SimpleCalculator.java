/** author: b07703125 Chia-Chien, Liu*/
/** Description:a simple calculator that keeps track of a number of type double and can operate repeatedly.*/
import java.text.DecimalFormat;

public class SimpleCalculator {
    private double result=0.0;
    private int count=0;
    private String cmd = "";
    private String operator;
    private double value;
    /**Parse the command and set new result. If command is not valid, throws an UnknownCmdException contained the error message.*/
    public void calResult(String cmd) throws UnknownCmdException {
        if(!endCalc(cmd)) {
            String[] container =  cmd.split(" ");
            /** invalid form*/
            if(container.length != 2) {
                throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
            }
            
            else if(isNumber(container[1])) {
                value = Double.parseDouble(container[1]);
                switch (container[0]) {
                    case "+":
                        result += value;
                        operator = "+";
                        count++;
                        break;
                    case "-":
                        result -= value;
                        operator = "-";
                        count++;
                        break;
                    case "*":
                        result *= value;
                        operator = "*";
                        count++;
                        break;
                    case "/":
                    	/** dividing by zero*/
                        if(value == 0) {
                            throw new UnknownCmdException("Can not divide by 0");
                        }
                        else {
                            result /= value;
                            operator = "/";
                            count++;
                        }
                        break;
                    /** invalid operator*/
                    default:
                        throw new UnknownCmdException(container[0] + " is an unknown operator");
                }
            }
            
            else {
            	/**invalid value*/
                if(container[0].equals("+") || container[0].equals("-") || container[0].equals("*") || container[0].equals("/")) {
                    throw new UnknownCmdException(container[1] + " is an unknown value");
                }
                /**both invalid*/
                else {
                    throw new UnknownCmdException(container[0] + " is an unknown operator and " + container[1] + " is an unknown value");
                }
            }
        }
    }
    /**The output message depends on the calculation count. Returns the result of the command, or throws an exception with an error message.*/
    public String getMsg() {
        DecimalFormat df = new DecimalFormat("###0.00");
        /** when the calculation finish*/
        if(endCalc(cmd)) {
            return "Final result = " + df.format(result);
        }
        /** when starting the calculation*/
        else if(count == 0) {
            return "Calculator is on. Result = 0.00";
        }
        /** when calculation count = 1*/
        else if(count == 1) {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". New result = " + df.format(result);
        }
        /** when calculation count >  1*/
        else {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". Updated result = " + df.format(result);
        }
    }
    /**determine whether calculation comes to the end. Returns true if cmd is "r" or "R", otherwise false.*/
    public boolean endCalc(String cmd) {
        if(cmd.equals("r") || cmd.equals("R")) {
            this.cmd = cmd;
            return true;
        }
        else {
            return false;
        }

    }
    /**In running calResult, determine if is valid form number*/
    public static boolean isNumber(String s){
        try{
            Double.parseDouble(s);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}