/** author: b07703125 Chia-Chien, Liu*/
/** errMessage is among five probable error messages mentioned.*/
public class UnknownCmdException extends Exception {
    public UnknownCmdException(String errMessage) {
        super(errMessage);
    }
}